.PHONY: clean build start log shell all
all: up log

env:=development
docker_compose_command:=docker-compose --file docker-compose-$(env).yml

ifeq ($(env),development)
# --force-rm --pull --no-cache
build_args:=
else
build_args:=
endif

up: stop pull build
	$(docker_compose_command) up --detach --remove-orphans

stop:
	$(docker_compose_command) stop --timeout 10

log:
	$(docker_compose_command) logs --follow --tail="all"

pull:
	$(docker_compose_command) pull --parallel

build:
	$(docker_compose_command) build --pull $(build_args)

clean: stop
	$(docker_compose_command) rm   --force --stop -v
	$(docker_compose_command) down --remove-orphans

worker:
	. venv/bin/activate;cd src;celery worker --app core --task-events --loglevel info --autoscale=20,0 --pidfile /tmp/celeryworker.pid

beat:
	. venv/bin/activate;cd src;celery beat   --app core --loglevel debug --scheduler django_celery_beat.schedulers:DatabaseScheduler --pidfile /tmp/celerybeat.pid

