from django.core.management.base import BaseCommand
from django.utils import timezone

from Profile.models import BackupModel, LogModel
from UI.tasks import run_backup


class Command(BaseCommand):
    def handle(self, *args, **options):
        for i in range(10):

            if i == 3:
                continue
            if i == 5:
                return False
            print(i)
