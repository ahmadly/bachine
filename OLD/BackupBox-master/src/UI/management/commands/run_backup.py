from django.core.management.base import BaseCommand
from django.utils import timezone

from Profile.models import BackupModel, LogModel

from salt.client import LocalClient
from salt.client.ssh.client import SSHClient


class Command(BaseCommand):
    def handle(self, *args, **options):
        for backup in BackupModel.objects.filter(enabled=True, deleted=False):

            _now = timezone.now()
            _log = LogModel(backup=backup, log=[])

            if backup.next_run_time < _now:
                if not backup.device.allow_concurrent_backup and backup.device.have_running_backup:
                    self.stdout.write(
                        "if not backup.device.allow_concurrent_backup and backup.device.have_running_backup:"
                    )
                    continue
                if _now.hour.__str__() in backup.escape_time:
                    self.stdout.write(
                        "elif _now.hour.__str__() in backup.escape_time:"
                    )
                    continue

                self.stdout.write('started')

                _concurrent_backup = False
                _client = None
                _cmd = None
                _kwarg = {}
                cmd_list = []

                if not backup.device.allow_concurrent_backup:
                    backup.device.have_running_backup = True
                    backup.save()
                    _log.log.append('backup.device.have_running_backup = True')
                    _concurrent_backup = True

                if backup.device.get_access_method_display() == 'SSH':
                    _client = SSHClient()
                if backup.device.get_access_method_display() == 'Minion':
                    _client = LocalClient()
                if backup.device.get_access_method_display() == 'Telnet':
                    _client = None

                if backup.get_backup_method_display() == 'MySQL':
                    cmd_list.append({'cmd': 'backup_mysql.dump_db',
                                     'arg': [],
                                     'kwarg': {'host': backup.config['config_mysql_host'],
                                               'database_name': backup.config['config_mysql_dbname'],
                                               'username': backup.config['config_mysql_username'],
                                               'password': backup.config['config_mysql_password'],
                                               'out_filename': '/tmp/{}.sql'.format(backup.id)
                                               }
                                     })
                    cmd_list.append({
                        'cmd': 'cp.push',
                        'arg': ['/tmp/{}.sql'.format(backup.id)],
                        'kwarg': {'remove_source': True,
                                  # 'upload_path': ''
                                  }
                    })

                elif backup.get_backup_method_display() == 'PostgreSQL':
                    cmd_list.append({
                        'cmd': 'backup_postgresql.dump_db',
                        'arg': [],
                        'kwarg ': {'host': backup.config['config_postgresql_host'],
                                   'database_name': backup.config['config_postgresql_dbname'],
                                   'username': backup.config['config_postgresql_username'],
                                   'password': backup.config['config_postgresql_password'],
                                   'out_filename': '/tmp/postgresql.sql'
                                   }

                    })

                for cmd in cmd_list:
                    _result = _client.cmd(backup.device.name, fun=cmd['cmd'], arg=cmd['arg'], kwarg=cmd['kwarg'])
                    _log.log.append(_result)

                if _concurrent_backup:
                    backup.device.have_running_backup = False
                    _log.log.append('backup.device.have_running_backup = False')

                # backup.next_run_time += timezone.timedelta(hours=backup.loop_interval)
                # _log.log.append(backup.next_run_time)

                _log.save()
                backup.save()
