from __future__ import absolute_import, unicode_literals
from multiprocessing import current_process

from django.utils import timezone
from salt.client import LocalClient
from salt.client.ssh.client import SSHClient

from Profile.models import DeviceModel, BackupModel


def run_backup(backup, log):
    # current_process().daemon = False
    _concurrent_backup = False
    client = None
    if not backup.device.allow_concurrent_backup:
        backup.device.have_running_backup = True
        backup.save()
        log.log.append('backup.device.have_running_backup = True')
        _concurrent_backup = True

    if backup.device.get_access_method_display() == 'SSH':
        # sudo salt-ssh --user='root' --passwd='vagrant' --roster=scan '10.57.3.102' test.ping
        # client.cmd(tgt='10.1.45.191', fun='test.ping', ssh_user='root', ssh_passwd='passwd', roster='scan')

        client = SSHClient()
        _result = client.cmd('*', 'cmd.run', ['ls', '/'])
        print(_result)
        print(dir(_result))
        log.log.append(str(_result))
    # if backup.device.get_access_method_display() == 'Minion':
    #     client = LocalClient()
    # if backup.device.get_access_method_display() == 'Telnet':
    #     return False

    if _concurrent_backup:
        backup.device.have_running_backup = False
        log.log.append('backup.device.have_running_backup = False')

    backup.next_run_time += timezone.timedelta(hours=backup.loop_interval)

    # backup.save()
    # log.save()

    return log.id.__str__()
