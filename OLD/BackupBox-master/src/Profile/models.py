import uuid

from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import JSONField, ArrayField
from django.contrib.auth.models import Group

from Profile.storage import BackupStorage
from Profile.tools import roster_file_delete, roster_file_update, upload_path_handler
from Profile.validators import file_size_validator


class DeviceModel(models.Model):
    # todo: Permission support
    group = models.ManyToManyField(to=Group)
    name = models.CharField(max_length=200, verbose_name='Device Name')
    ip = models.GenericIPAddressField(verbose_name='IP')
    enabled = models.BooleanField(default=False, verbose_name='Enabled', help_text='is device enabled to use ?')
    deleted = models.BooleanField(default=False, verbose_name='Deleted')
    allow_concurrent_backup = models.BooleanField(default=False)
    have_running_backup = models.BooleanField(default=False)

    config = JSONField(default={})

    access_method = models.PositiveIntegerField(choices=((1, 'SSH'), (2, 'Telnet'), (3, 'Minion')))
    type = models.PositiveIntegerField(
        choices=((1, 'Linux'), (2, 'Windows'), (3, 'Mikrotik'), (4, 'Cisco'), (5, 'DSLAM')))

    def save(self, *args, **kwargs):
        _name = self.name.strip().lower().replace(' ', '_')
        self.name = _name

        if self.get_access_method_display() == 'SSH':
            roster_file_delete(settings.ROSTER_FILE, self.name)
            roster_file_update(settings.ROSTER_FILE, {self.name: {'host': self.ip,
                                                                  'user': self.config['config_ssh_username'],
                                                                  'passwd': self.config['config_ssh_password'],
                                                                  'port': self.config['config_ssh_port'],
                                                                  'sudo': self.config['config_ssh_sudo'],
                                                                  'timeout': self.config['config_ssh_timeout'],
                                                                  }})

        super(DeviceModel, self).save(*args, **kwargs)

    def __str__(self):
        return self.name.__str__()


class BackupModel(models.Model):
    """
    user defined backup for every device
    each device should have multiple backup
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    device = models.ForeignKey(to=DeviceModel, limit_choices_to={'enabled': True, 'deleted': False})
    number_of_archives = models.PositiveIntegerField(default=10)
    loop_interval = models.PositiveIntegerField(default=24)
    next_run_time = models.DateTimeField()
    escape_time = ArrayField(models.CharField(max_length=10))

    enabled = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    config = JSONField(default={})

    script = ArrayField(models.TextField(blank=True, null=True), blank=True, null=True)

    backup_method = models.PositiveIntegerField(choices=(
        (1, 'File'),
        (2, 'Directory'),
        (3, 'Config'),
        (4, 'MySQL'),
        (5, 'PostgreSQL'),
        (6, 'SQLServer'),
    ))

    def __str__(self):
        return self.id.__str__()


class LogModel(models.Model):
    id = models.UUIDField(primary_key=True, verbose_name='ID', editable=False, default=uuid.uuid4)
    backup = models.ForeignKey(to=BackupModel, limit_choices_to={'enabled': True, 'deleted': False})

    result = models.NullBooleanField()
    verify = models.NullBooleanField()

    file = models.FileField(upload_to=upload_path_handler, storage=BackupStorage, validators=[file_size_validator],
                            blank=True, null=True)
    log = ArrayField(models.TextField())

    def __str__(self):
        return self.id.__str__()
