import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage

BackupStorage = FileSystemStorage(
    location=os.path.join(settings.MEDIA_ROOT, 'backup'),
    base_url=os.path.join(settings.MEDIA_URL, 'backup'),
)

