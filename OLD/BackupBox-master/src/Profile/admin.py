from django.contrib import admin
from Profile.models import DeviceModel, BackupModel, LogModel
from Profile.forms import DeviceForm, BackupForm  # , LogForm


class DeviceAdmin(admin.ModelAdmin):
    form = DeviceForm


class BackupAdmin(admin.ModelAdmin):
    form = BackupForm


# class LogAdmin(admin.ModelAdmin):
#     form = LogForm


admin.site.register(DeviceModel, DeviceAdmin)
admin.site.register(BackupModel, BackupAdmin)
admin.site.register(LogModel)
