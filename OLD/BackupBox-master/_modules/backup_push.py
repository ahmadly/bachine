# -*- coding: utf-8 -*-
"""
PGPASSWORD="password"  pg_dump testdb -U postgres -h localhost -f file.dump
"""
from __future__ import absolute_import

import salt.utils
import salt.utils.network

import logging

log = logging.getLogger(__name__)

__virtualname__ = 'backup_postgresql'

