# -*- coding: utf-8 -*-
'''
Compendium of generic DNS utilities.
The 'dig' command line tool must be installed in order to use this module.
'''
from __future__ import absolute_import

# Import salt libs
import salt.utils
import salt.utils.network

# Import python libs
import logging

log = logging.getLogger(__name__)

__virtualname__ = 'backup_mysql'


def __virtual__():
    '''
    Only load module if mysqldump binary is present
    '''
    if salt.utils.which('mysqldump'):
        return __virtualname__
    return (False, 'The mysqldump execution module cannot be loaded: '
                   'the mysqldump binary is not in the path.')


def dump_db(host, database_name, username, password, out_filename):
    mysqldump = ['mysqldump',
                 '-h{}'.format(host),
                 '-u{}'.format(username),
                 '-p{}'.format(password),
                 '{}'.format(database_name),
                 '--result-file={}'.format(out_filename),
                 ]

    cmd = __salt__['cmd.run_all'](mysqldump, python_shell=False)

    if cmd['retcode'] != 0:
        log.warning('fallback. {}'.format(cmd['retcode']))
        return False

    return True


def dump_all_db(host, username, password, out_filename):
    mysqldump = ['mysqldump',
                 '-h{}'.format(host),
                 '-u{}'.format(username),
                 '-p{}'.format(password),
                 '--all-databases',
                 '--result-file={}'.format(out_filename),
                 ]

    cmd = __salt__['cmd.run_all'](mysqldump, python_shell=False)

    if cmd['retcode'] != 0:
        log.warning('fallback. {}'.format(cmd['retcode']))
        return False

    return True
