# -*- coding: utf-8 -*-
"""
PGPASSWORD="password"  pg_dump testdb -U postgres -h localhost -f file.dump
"""

from __future__ import absolute_import

import salt.utils
import salt.utils.network

import logging

log = logging.getLogger(__name__)

__virtualname__ = 'backup_postgresql'


def __virtual__():
    """
    Only load module if dig binary is present
    """
    if salt.utils.which('pg_dump'):
        return __virtualname__
    return (False, 'The pg_dump execution module cannot be loaded: '
                   'the pg_dump binary is not in the path.')


def dump_db(host, database_name, username, password, out_filename):
    pg_dump = [
        'pg_dump',
        '--dbname={}'.format(database_name),
        '--username={}'.format(username),
        '--host={}'.format(host),
        '--file={}'.format(out_filename),
    ]

    cmd = __salt__['cmd.run_all'](pg_dump, python_shell=False,
                                  env=[{'PGPASSWORD': password}]
                                  )

    if cmd['retcode'] != 0:
        log.warning('fallback. {}'.format(cmd['retcode']))
        return False

    return True


def dump_all_db(host, username, password, out_filename):
    pg_dump = [
        'pg_dumpall',
        '--username={}'.format(username),
        '--host={}'.format(host),
        '--clean',
        '--file={}'.format(out_filename),
    ]

    cmd = __salt__['cmd.run_all'](pg_dump, python_shell=False,
                                  env=[{'PGPASSWORD': password}]
                                  )

    if cmd['retcode'] != 0:
        log.warning('fallback. {}'.format(cmd['retcode']))
        return False

    return True
