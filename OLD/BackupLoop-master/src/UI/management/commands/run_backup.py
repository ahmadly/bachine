from django.core.management.base import BaseCommand
from django.utils import timezone

from Profile.models import BackupModel
from UI.tasks import run_backup


class Command(BaseCommand):
    def handle(self, *args, **options):
        for backup in BackupModel.objects.filter(enabled=True, deleted=False):

            _now = timezone.now()

            if backup.next_run_time < _now:
                if not backup.device.allow_concurrent_backup and backup.device.have_running_backup:
                    self.stdout.write(
                        "if not backup.device.allow_concurrent_backup and backup.device.have_running_backup:"
                    )
                    continue
                if _now.hour.__str__() in backup.escape_time:
                    self.stdout.write(
                        "elif _now.hour.__str__() in backup.escape_time:"
                    )
                    continue

                # run task
                _id = run_backup.delay(backup)
                backup.save()

                self.stdout.write("Backup ID {} Started.".format(_id.task_id))
