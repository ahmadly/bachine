from __future__ import absolute_import, unicode_literals

import subprocess
from django.utils import timezone
from celery import shared_task

from Profile.models import LogModel


@shared_task(serializer='pickle')
def run_backup(backup):
    _report = LogModel(backup=backup, log=[])
    _report.save()
    if not backup.device.allow_concurrent_backup:
        backup.device.have_running_backup = True
        backup.save()

    command = subprocess.Popen(backup.cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    try:
        # todo set timeout from variable
        out, error = command.communicate(timeout=600)

        if command.returncode != 0:
            _report.log.append(str(error))
            _report.result = False

        else:
            _report.log.append(str(out))
            _report.result = True

        _report.log.append(str(backup.cmd))

    except subprocess.TimeoutExpired:
        command.kill()
        _report.result = False
        _report.log.append('time out')

    except Exception as UnExceptionalError:
        _report.log.append(str(UnExceptionalError))
        command.kill()
        _report.result = True

    backup.device.have_running_backup = False
    backup.next_run_time += timezone.timedelta(hours=backup.loop_time)
    backup.save()
    _report.save()

    return _report.id.__str__()
