from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse
from django.shortcuts import render
from Profile.models import DeviceModel, BackupModel, LogModel
from Profile.forms import DeviceForm
from Profile.storage import BackupStorage
from django.core.files.base import File


@require_http_methods(["POST"])
@csrf_exempt
def file_receiver(request):
    backup = request.FILES['backup']
    backup_id = request.POST['backup_id']
    file_size = request.POST['file_size']
    file_md5 = request.POST['file_md5']

    backup_obj = BackupModel.objects.get(id=backup_id)

    # todo : i think have problem when  have multiple backup
    log = LogModel.objects.filter(backup=backup_obj, result=True, verify=None).last()

    log.file = backup

    log.log.append('file_size:{}'.format(file_size))
    log.log.append('file_md5:{}'.format(file_md5))

    log.save()

    return HttpResponse('log id:{}'.format(log.id))


def get_script(request, backup_id):
    _backup = BackupModel.objects.get(id=backup_id)
    return HttpResponse(_backup.script)
