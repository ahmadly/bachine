from django.core.exceptions import ValidationError


def file_size_validator(file):
    if file.size < 1000:
        raise ValidationError('file too small : %(size)s ', params={'size': file.size})
