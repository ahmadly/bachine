from django import forms
from Profile.models import DeviceModel, BackupModel, LogModel
from Profile.tools import CHOICES_HOURS


class DeviceForm(forms.ModelForm):
    config_minion_name = forms.CharField(required=False, label='Minion Name')

    config_ssh_username = forms.CharField(required=False, label='SSH UserName')
    config_ssh_password = forms.CharField(required=False, label='SSH PassWord')
    config_ssh_port = forms.IntegerField(required=False, label='SSH Port', initial=22)
    config_ssh_sudo = forms.BooleanField(required=False, label='Allow `sudo`')
    config_ssh_timeout = forms.IntegerField(required=False, label='SSH TimeOut', initial=600)

    config_telnet_username = forms.CharField(required=False, label='Telnet UserName')
    config_telnet_password = forms.CharField(required=False, label='Telnet PassWord')
    config_telnet_port = forms.CharField(required=False, label='Telnet Port', initial=23)
    config_telnet_timeout = forms.IntegerField(required=False, label='Telnet TimeOut', initial=600)

    class Meta:
        model = DeviceModel
        fields = ['group', 'name', 'ip', 'enabled', 'deleted',
                  'access_method', 'allow_concurrent_backup',
                  'have_running_backup', 'type']

    def clean(self):
        super(DeviceForm, self).clean()

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', {})
        instance = kwargs.get('instance', None)

        if instance:
            for field in self.base_fields:
                if field.startswith('config'):
                    initial[field] = instance.config[field] if field in instance.config else None

        kwargs['initial'] = initial
        kwargs['instance'] = instance
        super(DeviceForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(DeviceForm, self).save(commit=False)
        for k in self.cleaned_data:
            if k.startswith('config'):
                instance.config[k] = self.cleaned_data[k]
        if commit:
            instance.save()

        return instance


class BackupForm(forms.ModelForm):
    config_file = forms.CharField(required=False, label='File name')

    config_mysql_username = forms.CharField(required=False, label='MySQL User Name')
    config_mysql_password = forms.CharField(required=False, label='MySQL Password')
    config_mysql_dbname = forms.CharField(required=False, label='MySQL DB Name')
    config_mysql_port = forms.IntegerField(required=False, label='MySQL Port')

    config_postgresql_username = forms.CharField(required=False, label='Postgresql User Name')
    config_postgresql_password = forms.CharField(required=False, label='Postgresql Password')
    config_postgresql_dbname = forms.CharField(required=False, label='Postgresql DB Name')
    config_postgresql_port = forms.IntegerField(required=False, label='Postgresql Port')

    config_mssql_username = forms.CharField(required=False, label='MS SQL Server User Name')
    config_mssql_password = forms.CharField(required=False, label='MS SQL Server Password')
    config_mssql_dbname = forms.CharField(required=False, label='MS SQL Server DB Name')
    config_mssql_port = forms.IntegerField(required=False, label='MS SQL Server Port')

    escape_time = forms.MultipleChoiceField(choices=CHOICES_HOURS, widget=forms.SelectMultiple())

    class Meta:
        model = BackupModel
        fields = ['device', 'keep_files_number',
                  'loop_time', 'next_run_time', 'enabled',
                  'deleted', 'cmd', 'custom_cmd', 'script', 'escape_time',
                  'backup_method']

    def __init__(self, *args, **kwargs):
        initial = kwargs.get('initial', {})
        instance = kwargs.get('instance', None)

        if instance:
            for field in self.base_fields:
                if field.startswith('config'):
                    initial[field] = instance.config[field] if field in instance.config else None

        kwargs['initial'] = initial
        kwargs['instance'] = instance
        super(BackupForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(BackupForm, self).save(commit=False)
        for k in self.cleaned_data:
            if k.startswith('config'):
                instance.config[k] = self.cleaned_data[k]
        if commit:
            instance.save()

        return instance


class LogForm(forms.ModelForm):
    class Meta:
        model = LogModel
        fields = ['backup', 'result', 'verify', 'file', 'log']
